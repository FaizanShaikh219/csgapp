package djf;

import djf.ui.*;
import djf.components.*;
import djf.modules.AppFoolproofModule;
import djf.modules.AppLanguageModule;
import javafx.application.Application;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import static djf.settings.AppPropertyType.*;
import djf.settings.AppStartupConstants;
import static djf.settings.AppStartupConstants.*;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import properties_manager.InvalidXMLFileFormatException;


public abstract class AppTemplate extends Application {

    // THIS IS THE APP'S FULL JavaFX GUI. NOTE THAT ALL APPS WOULD
    // SHARE A COMMON UI EXCEPT FOR THE CUSTOM WORKSPACE
    protected AppGUI gui;

    // THIS CLASS USES A COMPONENT ARCHITECTURE DESIGN PATTERN, MEANING IT
    // HAS OBJECTS THAT CAN BE SWAPPED OUT FOR OTHER COMPONENTS
    // THIS APP HAS 4 COMPONENTS
    public static final String PATH_DATA = "./data/";
    
    /**
     * Directory for all properties files, including app_properties.xml,
     * which has all the non-language settings for loading the 
     * application, and all the language-specific properties files
     * that are used to load text into UI controls whenever the 
     * application language changes.
     */
    public static final String PATH_PROPERTIES = PATH_DATA + "properties/";
    
    /**
     * Directory for storing the recent work file, which keeps track
     * of which files were edited most recently.
     */
    public static final String PATH_RECENT = PATH_DATA + "recent/";
    
    /**
     * Directory for storing Web pages for the Help and About screens
     * as well as any Web pages or Web templates needed by the
     * particular application.
     */
    public static final String PATH_WEB = PATH_DATA + "web/";
    
    /**
     * Directory where user work is saved by default.
     */
    public static final String PATH_WORK = "./work/";
    public static final String PATH_ICONS = "./images/icons/";
    public static final String PATH_TEMP = "./temp/";
    
    // THE COMPONENT FOR MANAGING CUSTOM APP DATA
    protected AppDataComponent dataComponent;
    
    // THE COMPONENT FOR MANAGING CUSTOM FILE I/O
    protected AppFileComponent fileComponent;
    protected AppLanguageModule     languageModule = new AppLanguageModule(this);
    protected AppFoolproofModule    foolproofModule = new AppFoolproofModule(this);
    
    // THE COMPONENT FOR THE GUI WORKSPACE
    protected AppWorkspaceComponent workspaceComponent;
    
    // THE COMPONENT FOR MANAGING UI STYLE
    protected AppStyleComponent styleComponent;
        
    // THIS METHOD MUST BE OVERRIDDEN WHERE THE CUSTOM BUILDER OBJECT
    // WILL PROVIDE THE CUSTOM APP COMPONENTS

    /**
     * This function must be overridden, it should initialize all
     * of the components used by the app in the proper order according
     * to the particular app's dependencies.
     */
    public abstract void buildAllApplicationComponents();
    public AppLanguageModule getLanguageModule() {
        return languageModule;
    }
    public AppFoolproofModule getFoolproofModule() {
        return foolproofModule;
    }
    // COMPONENT ACCESSOR METHODS

    /**
     *  Accessor for the data component.
     */
    public AppDataComponent getDataComponent() { return dataComponent; }
    
    /**
     *  Accessor for the file component.
     */
    public AppFileComponent getFileComponent() { return fileComponent; }

    /**
     *  Accessor for the workspace component.
     */
    public AppWorkspaceComponent getWorkspaceComponent() { return workspaceComponent; }
    
    /**
     *  Accessor for the style component.
     */
    public AppStyleComponent getStyleComponent() { return styleComponent; }

    /**
     *  Accessor for the gui. Note that the GUI would contain the workspace.
     */
    public AppGUI getGUI() { return gui; }

    /**
     * This is where our Application begins its initialization, it will load
     * the custom app properties, build the components, and fully initialize
     * everything to get the app rolling.
     *
     * @param primaryStage This application's window.
     */
    @Override
    public void start(Stage primaryStage) {
        
 
	ChooseDialog yesNoDialog = ChooseDialog.getSingleton();
	yesNoDialog.init(primaryStage);
	PropertiesManager props = PropertiesManager.getPropertiesManager();

	try {
            
            boolean success;
	    
        	    success = loadProperties(APP_PROPERTIES_FILE_NAME);
                    
            
            
	    if (success) {
                
		String appTitle = props.getProperty(APP_TITLE);

                
		gui = new AppGUI(primaryStage, appTitle, this);

              
		buildAllApplicationComponents();
               

	    } 
	}catch (Exception e) {
            e.printStackTrace();
            MsgDialog dialog = MsgDialog.getSingleton();
            dialog.show(props.getProperty(PROPERTIES_LOAD_ERROR_TITLE), props.getProperty(PROPERTIES_LOAD_ERROR_MESSAGE));
	}
    }
    
   
    public static boolean loadProperties(String propertiesFileName) {
	    PropertiesManager props = PropertiesManager.getPropertiesManager();
            
        try {
            //System.out.println("h");
            props.addProperty(PropertiesManager.DATA_PATH_PROPERTY, PATH_DATA);
            props.loadProperties(propertiesFileName,PROPERTIES_SCHEMA_FILE_NAME);
            return true;
        } catch (InvalidXMLFileFormatException ex) {
            Logger.getLogger(AppTemplate.class.getName()).log(Level.SEVERE, null, ex);
        }
            return true;
           
    }
}