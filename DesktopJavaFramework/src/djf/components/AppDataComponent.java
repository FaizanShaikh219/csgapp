package djf.components;


public interface AppDataComponent {

    /**
     * This function would be called when initializing data.
     */
    public void resetData();
}
