package djf.components;

import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;


public abstract class AppWorkspaceComponent {
    // THIS IS THE WORKSPACE WHICH WILL BE DIFFERENT
    // DEPENDING ON THE CUSTOM APP USING THIS FRAMEWORK

    // THIS IS THE MAIN WORKSPACE PANE, ALL OTHER CONTROLS 
    // WOULD GO INSIDE. NOTE THAT WHEN IT IS CONSTRUCTED,
    // IT MAY ACTUALLY BE ANY Pane DESCENDENT CLASS
    protected Pane workspace;
    protected BorderPane appPane;
    // THIS DENOTES THAT THE USER HAS BEGUN WORKING AND
    // SO THE WORKSPACE IS VISIBLE AND USABLE
    protected boolean workspaceActivated;
    
    /**
     * When called this function puts the workspace into the window,
     * revealing the controls for editing work.
     * 
     * @param appPane The pane that contains all the controls in the
     * entire application, including the file toolbar controls, which
     * this framework manages, as well as the customly provided workspace,
     * which would be different for each app.
     */
    public void activateWorkspace(BorderPane appPane) {
        if (!workspaceActivated) {
            // PUT THE WORKSPACE IN THE GUI
            this.appPane=appPane;
            this.appPane.setLeft(null);
            this.appPane.setRight(null);
            this.appPane.setBottom(null);
            appPane.setCenter(workspace);
            workspaceActivated = true;
        }
    }
    public void deactivate() {
        if (workspaceActivated) {
            // REMOVE THE WORKSPACE FROM THE GUI
            this.appPane=appPane;
            appPane.setCenter(null);            
            workspaceActivated = false;
        }
    }
    
    /**
     * Mutator method for setting the custom workspace.
     * 
     * @param initWorkspace The workspace to set as the user
     * interface's workspace.
     */
    public void setWorkspace(Pane initWorkspace) { 
	workspace = initWorkspace; 
    }
    
    /**
     * Accessor method for getting the workspace.
     * 
     * @return The workspace pane for this app.
     */
    public Pane getWorkspace() { return workspace; }
    
    // THE DEFINITION OF THIS CLASS SHOULD BE PROVIDED
    // BY THE CONCRETE WORKSPACE

    /**
     * This function must be defined in the actual workspace
     * component class and should be called before loading
     * new data.
     */
    public abstract void resetWorkspace();

    /**
     * This function must be defined in the actual workspace
     * component class and should be called after data has
     * been loaded and the workspace must use it to initialize
     * controls.
     */
    public abstract void reloadWorkspace(AppDataComponent dataComponent);
    
    public abstract void undoAction();
    public abstract void redoAction();

    public abstract void showAppInfo();
    public abstract void changeAction();
    public abstract void helpAction();

    public abstract void aboutAction();
    
}
