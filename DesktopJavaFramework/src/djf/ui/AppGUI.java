package djf.ui;
//import org.apache.commons.io.FileUtils;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;
import javafx.stage.Screen;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import djf.controller.AppFileController;
import djf.AppTemplate;
import static djf.AppTemplate.PATH_WORK;
import static djf.settings.AppPropertyType.*;
import static djf.settings.AppStartupConstants.FILE_PROTOCOL;
import static djf.settings.AppStartupConstants.PATH_IMAGES;
import static djf.components.AppStyleComponent.CLASS_BORDERED_PANE;
import static djf.components.AppStyleComponent.CLASS_FILE_BUTTON;
import djf.controller.AppHelpController;
import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;


public class AppGUI {
    // THIS HANDLES INTERACTIONS WITH FILE-RELATED CONTROLS
    protected AppFileController fileController;
protected AppHelpController helpController;
    // THIS IS THE APPLICATION WINDOW
    protected Stage primaryStage;
protected HashMap<Object, Stage> dialogs;
    // THIS IS THE STAGE'S SCENE GRAPH
    protected Scene primaryScene;

    // THIS PANE ORGANIZES THE BIG PICTURE CONTAINERS FOR THE
    // APPLICATION AppGUI. NOTE THAT THE WORKSPACE WILL GO
    // IN THE CENTER REGION OF THE appPane
    protected BorderPane appPane;
    protected AppNodesBuilder nodesBuilder;
    protected HashMap<String, Node> guiNodes;
    // THIS IS THE TOP TOOLBAR AND ITS CONTROLS
    protected FlowPane fileToolbarPane;

    // FILE TOOLBAR BUTTONS
    protected Button newButton;
    protected Button undoButton;
    protected Button redoButton;
    protected Button loadButton;
    protected Button saveButton;
    protected Button closeButton;
    protected Button saveAsButton;
    protected Button exportButton;
    protected Button exitButton;
    protected Button changeButton;
    protected Button helpButton;
    protected Button aboutButton;
    
    
    // THIS DIALOG IS USED FOR GIVING FEEDBACK TO THE USER
    protected ChooseDialog yesNoCancelDialog;
    public static final boolean ENABLED = true;
    public static final boolean DISABLED = false;
    // THIS TITLE WILL GO IN THE TITLE BAR
    protected String appTitle;
    
    /**
     * This constructor initializes the file toolbar for use.
     * 
     * @param initPrimaryStage The window for this application.
     * 
     * @param initAppTitle The title of this application, which
     * will appear in the window bar.
     * 
     * @param app The app within this gui is used.
     */
    public AppGUI(  Stage initPrimaryStage, 
		    String initAppTitle, 
		    AppTemplate app){
	// SAVE THESE FOR LATER
	primaryStage = initPrimaryStage;
	appTitle = initAppTitle;
	guiNodes = new HashMap();
        dialogs = new HashMap();

        // THIS WILL BUILD ALL THE NODES
        nodesBuilder = new AppNodesBuilder(app.getGUI(), app.getLanguageModule());
        // INIT THE TOOLBAR
        initFileToolbar(app);
		
        // AND FINALLY START UP THE WINDOW (WITHOUT THE WORKSPACE)
        initWindow();
    }
    
    /**
     * Accessor method for getting the file toolbar controller.
     */
    public AppFileController getFileController() { return fileController; }
    public void addGUINode(Object nodeId, Node nodeToAdd) {
        guiNodes.put(nodeId.toString(), nodeToAdd);
    }
    public Node getGUINode(Object nodeId) {
        return guiNodes.get(nodeId.toString());
    }
    /**
     * Accessor method for getting the application pane, within which all
     * user interface controls are ultimately placed.
     * 
     * @return This application GUI's app pane.
     */
    public BorderPane getAppPane() { return appPane; }
    
    /**
     * Accessor method for getting this application's primary stage's,
     * scene.
     * 
     * @return This application's window's scene.
     */
    public Scene getPrimaryScene() { return primaryScene; }
    
    /**
     * Accessor method for getting this application's window,
     * which is the primary stage within which the full GUI will be placed.
     * 
     * @return This application's primary stage (i.e. window).
     */    
    public Stage getWindow() { return primaryStage; }

    /**\
     * This method is used to activate/deactivate toolbar buttons when
     * they can and cannot be used so as to provide foolproof design.
     * 
     * @param saved Describes whether the loaded Page has been saved or not.
     */
    public void updateToolbarControls(boolean saved) {
        // THIS TOGGLES WITH WHETHER THE CURRENT COURSE
        // HAS BEEN SAVED OR NOT
        //saveButton.setDisable(saved);
        //saveAsButton.setDisable(false);

        // ALL THE OTHER BUTTONS ARE ALWAYS ENABLED
        // ONCE EDITING THAT FIRST COURSE BEGINS
	newButton.setDisable(false);
        loadButton.setDisable(false);
	exitButton.setDisable(false);

        // NOTE THAT THE NEW, LOAD, AND EXIT BUTTONS
        // ARE NEVER DISABLED SO WE NEVER HAVE TO TOUCH THEM
    }
    
    public void updateExportControl(boolean pathFound) {
        //exportButton.setDisable(!pathFound);
    }

    /****************************************************************************/
    /* BELOW ARE ALL THE PRIVATE HELPER METHODS WE USE FOR INITIALIZING OUR AppGUI */
    /****************************************************************************/
    
    /**
     * This function initializes all the buttons in the toolbar at the top of
     * the application window. These are related to file management.
     */
    private void initFileToolbar(AppTemplate app) {
        helpController = new AppHelpController(app);
        fileToolbarPane = new FlowPane();

        // HERE ARE OUR FILE TOOLBAR BUTTONS, NOTE THAT SOME WILL
        // START AS ENABLED (false), WHILE OTHERS DISABLED (true)
        newButton = initChildButton(fileToolbarPane,	NEW_ICON.toString(),	    NEW_TOOLTIP.toString(),	false); 
        
        loadButton = initChildButton(fileToolbarPane,	LOAD_ICON.toString(),	    LOAD_TOOLTIP.toString(),	false);
        saveButton = initChildButton(fileToolbarPane,	SAVE_ICON.toString(),	    SAVE_TOOLTIP.toString(),	true);
        
        //saveAsButton = initChildButton(fileToolbarPane,	SAVE_AS_ICON.toString(),	    SAVE_AS_TOOLTIP.toString(),	true);
        undoButton = initChildButton(fileToolbarPane,	UNDO_ICON.toString(),	    UNDO_TOOLTIP.toString(),	false); 
        redoButton = initChildButton(fileToolbarPane,	REDO_ICON.toString(),	    REDO_TOOLTIP.toString(),	false); 
        closeButton = initChildButton(fileToolbarPane,	CLOSE_ICON.toString(),	    CLOSE_TOOLTIP.toString(),	false);
        exportButton = initChildButton(fileToolbarPane,	EXPORT_ICON.toString(),	    EXPORT_TOOLTIP.toString(),	false);
        exitButton = initChildButton(fileToolbarPane,	EXIT_ICON.toString(),	    EXIT_TOOLTIP.toString(),	false);
     


        changeButton = initChildButton(fileToolbarPane,	CHANGE_ICON.toString(),	    CHANGE_TOOLTIP.toString(),	false);
        helpButton = initChildButton(fileToolbarPane,	HELP_ICON.toString(),	    HELP_TOOLTIP.toString(),	false);
        aboutButton = initChildButton(fileToolbarPane, ABOUT_ICON.toString(),	    ABOUT_TOOLTIP.toString(),	false);

    
        
	// and now setup their event handlers
        fileController = new AppFileController(app);
        newButton.setOnAction(e -> {
            //this.printfiles();
            
            saveButton.setDisable(false);
            
            fileController.handleNewRequest();
            loadButton.setDisable(true);
            newButton.setDisable(true);
        });
        undoButton.setOnAction(e -> {
            fileController.handleUndoRequest();
        });
        redoButton.setOnAction(e -> {
            fileController.handleRedoRequest();
        });
        loadButton.setOnAction(e -> {
            saveButton.setDisable(false);
            fileController.handleLoadRequest();
            loadButton.setDisable(true);
            newButton.setDisable(true);
        });
        closeButton.setOnAction(e -> {
            loadButton.setDisable(false);
            newButton.setDisable(false);
            fileController.handleCloseRequest();
            saveButton.setDisable(true);
        });
        saveButton.setOnAction(e -> {
            fileController.handleSaveRequest();
        });
        exitButton.setOnAction(e -> {
            fileController.handleExitRequest();
        });
        
        
        
        exportButton.setOnAction(e -> {
            fileController.handleExportRequest();

        });
        changeButton.setOnAction(e -> {
            helpController.processLanguageRequest();
        });
        helpButton.setOnAction(e -> {
            fileController.handleHelpRequest();
        });
        aboutButton.setOnAction(e -> {
            fileController.handleAboutRequest();
        });        

    }

    // INITIALIZE THE WINDOW (i.e. STAGE) PUTTING ALL THE CONTROLS
    // THERE EXCEPT THE WORKSPACE, WHICH WILL BE ADDED THE FIRST
    // TIME A NEW Page IS CREATED OR LOADED
    private void initWindow() {
        // SET THE WINDOW TITLE
        primaryStage.setTitle(appTitle);

        // GET THE SIZE OF THE SCREEN
        Screen screen = Screen.getPrimary();
        Rectangle2D bounds = screen.getVisualBounds();

        // AND USE IT TO SIZE THE WINDOW
        primaryStage.setX(bounds.getMinX());
        primaryStage.setY(bounds.getMinY());
        primaryStage.setWidth(bounds.getWidth());
        primaryStage.setHeight(bounds.getHeight());

        // ADD THE TOOLBAR ONLY, NOTE THAT THE WORKSPACE
        // HAS BEEN CONSTRUCTED, BUT WON'T BE ADDED UNTIL
        // THE USER STARTS EDITING A COURSE
        appPane = new BorderPane();
        appPane.setTop(fileToolbarPane);
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        //String desPathplus = System.getProperty("user.dir").toString()+"./images/CSG.png";
        //Image desImage1 = new Image(desPathplus);
        //ImageView desView1 = new ImageView(desImage1);
        //desView1.setFitHeight(50);
        //desView1.setFitWidth(50);
        VBox recent=new VBox(5);
        
        File dir = new File(System.getProperty("user.dir")+PATH_WORK+"/saved/");
        File[] directoryListing = dir.listFiles();
        if (directoryListing != null) {
            Label l =(new Label("Recent Works"));
            l.setPrefSize(300, 50);
            recent.getChildren().add(l);
            for (File child : directoryListing) {
                Button b=new Button(child.getName());
                b.setPrefSize(400, 50);
                b.setOnMouseClicked(e->{
                    saveButton.setDisable(false);
                    fileController.promptToOpen2(child);
                    loadButton.setDisable(true);
                    newButton.setDisable(true);
                });
                recent.getChildren().add(b);
                System.out.println(child.getName());
            }
        } 
        else {
        }
        appPane.setLeft(new ImageView(new Image(FILE_PROTOCOL + PATH_IMAGES + props.getProperty(APP_WELCOME))));
        Button cn=new Button("Create New Site");
        cn.setPrefSize(1300, 150);
        appPane.setBottom(cn);
        appPane.setRight(recent);
        cn.setOnMouseClicked(e->{
            saveButton.setDisable(false);
            
            fileController.handleNewRequest();
            loadButton.setDisable(true);
            newButton.setDisable(true);
        });
        primaryScene = new Scene(appPane);
        
        // SET THE APP ICON
	
        String appIcon = FILE_PROTOCOL + PATH_IMAGES + props.getProperty(APP_LOGO);
        primaryStage.getIcons().add(new Image(appIcon));

        // NOW TIE THE SCENE TO THE WINDOW AND OPEN THE WINDOW
        primaryStage.setScene(primaryScene);
        primaryStage.show();
    }
    
   
    public Button initChildButton(Pane toolbar, String icon, String tooltip, boolean disabled) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
	
	// LOAD THE ICON FROM THE PROVIDED FILE
        String imagePath = FILE_PROTOCOL + PATH_IMAGES + props.getProperty(icon);
        Image buttonImage = new Image(imagePath);
	
	// NOW MAKE THE BUTTON
        Button button = new Button();
        button.setDisable(disabled);
        ImageView buttonView = new ImageView(buttonImage);
        buttonView.setFitHeight(20);
        buttonView.setFitWidth(20);
        button.setGraphic(buttonView);
        
        Tooltip buttonTooltip = new Tooltip(props.getProperty(tooltip));
        button.setTooltip(buttonTooltip);
                                                                                                                                                                
	// PUT THE BUTTON IN THE TOOLBAR
        toolbar.getChildren().add(button);
	
	// AND RETURN THE COMPLETED BUTTON
        return button;
    }
    
    /**
     * This function specifies the CSS style classes for the controls managed
     * by this framework.
     */
    public void initFileToolbarStyle() {
	fileToolbarPane.getStyleClass().add(CLASS_BORDERED_PANE);
	newButton.getStyleClass().add(CLASS_FILE_BUTTON);
        undoButton.getStyleClass().add(CLASS_FILE_BUTTON);
        redoButton.getStyleClass().add(CLASS_FILE_BUTTON);
	loadButton.getStyleClass().add(CLASS_FILE_BUTTON);
	saveButton.getStyleClass().add(CLASS_FILE_BUTTON);
        closeButton.getStyleClass().add(CLASS_FILE_BUTTON);
//        saveAsButton.getStyleClass().add(CLASS_FILE_BUTTON);
        exportButton.getStyleClass().add(CLASS_FILE_BUTTON);
	exitButton.getStyleClass().add(CLASS_FILE_BUTTON);
        changeButton.getStyleClass().add(CLASS_FILE_BUTTON);

        helpButton.getStyleClass().add(CLASS_FILE_BUTTON);
	aboutButton.getStyleClass().add(CLASS_FILE_BUTTON);

    }

    public AppNodesBuilder getNodesBuilder() {
        return nodesBuilder;
    }

    

    
}